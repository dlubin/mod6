<?php
header("Content-Type: application/json"); 
 /**
 * @class SessionDB
 * Fake Database.  Stores records in $_SESSION
 */

//$user = $_SESSION['user'];
//$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'mod6');

class SessionDB {
    public function __construct() {
        if (!isset($_SESSION['pk'])) {
            //$_SESSION['pk'] = $this->pk();         // <-- start fake pks at 1020
            $_SESSION['rs'] = getData();    // <-- populate $_SESSION with data.
        }
    }
    // fake a database pk
    public function pk() {
        //return $_SESSION['pk']++;
/*	$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'mod6');
        if($mysqli->connect_errno) {
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
        }

	$stmt = $mysqli->prepare("SELECT LAST_INSERT_ID()");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt->execute();
	$stmt->bind_result($tem_id);
	while($stmt->fetch()){
		$_SESSION['pk'] = $tem_id;
	}
*/
	return $_SESSION['pk'];
    }
    // fake a resultset
    public function rs() {
        //return $_SESSION['rs'];
	 return getData();
    }
    
    public function insert($rec) {
	//var_dump($rec);
	$user = $_SESSION['user'];
	$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'mod6');
	if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
	}
	//array_push($_SESSION['rs'], $rec);
	$stmt = $mysqli->prepare("insert into events (name,cid,title,start,end,loc,url,rem,ad,notes) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt -> bind_param('sisssssiss',$user,$rec['cid'],$rec['title'],$rec['start'],$rec['end'],$rec['loc'],$rec['url'],$rec['rem'],$rec['ad'], $rec['notes']);
	$stmt -> execute();
	
        $stmt = $mysqli->prepare("SELECT LAST_INSERT_ID()");
        if(!$stmt){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
        }
        $stmt->execute();

        $stmt->bind_result($tem_id);
        while($stmt->fetch()){
                $_SESSION['pk'] = $tem_id;
        }
	
	$stmt -> close();
    }
    public function update($idx, $rec) {
        //$_SESSION['rs'][$idx] = $rec;
//	$user = $_SESSION['user'];
        $mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'mod6');
        if($mysqli->connect_errno) {
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
        }
        //array_push($_SESSION['rs'], $rec);
        $stmt = $mysqli->prepare("update events set cid = ?, title = ? ,start = ?,end = ? ,loc = ? ,url = ? ,rem = ?,ad = ?, notes = ? where id = ?");
        if(!$stmt){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
        }
        $stmt -> bind_param('isssssissi',$rec['cid'],$rec['title'],$rec['start'],$rec['end'],$rec['loc'],$rec['url'],$rec['rem'],$rec['ad'], $rec['notes'], $idx);
	$stmt -> execute();
	$stmt -> close();



    }
    public function destroy($idx) {
        //return array_shift(array_splice($_SESSION['rs'], $idx, 1));

        $user = $_SESSION['user'];
        $mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'mod6');
        if($mysqli->connect_errno) {
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
        }
        //array_push($_SESSION['rs'], $rec);
        $stmt = $mysqli->prepare("DELETE FROM events WHERE id= ?;");
        if(!$stmt){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
        }
        $stmt -> bind_param('i',$idx);
        $stmt -> execute();
        $stmt -> close();
		
	return getData();	
    }
}

function getDT($format) {
	return date('c', strtotime(date('Y-m-d', strtotime(date('Y-m-d'))).$format));
}

function getData() {
	// Load the default starting data. Should match the data in examples/calendar/event-list.js
	$user = $_SESSION['user'];
        $mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'mod6');
        if($mysqli->connect_errno) {
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
        }

	$stmt = $mysqli->prepare("select * from events where name = ?");
	if(!$stmt){
	    printf("Query Prep Failed: %s\n", $mysqli->error);
	    exit;
	}
	$stmt->bind_param('s', $user);
	$stmt->execute();
	$result = $stmt->get_result();
	
	$arr = array();
	$bigArr=array();
	while($row = $result->fetch_assoc()){
		$event_id = $row['id'];
		$cal_id =  $row['cid'];
		$title =  $row['title'];
		$start =  $row['start'];
		$end =  $row['end'];
		
		if(isset($row['loc'])){
		$location =  $row['loc'];
		}
		else{
			$location = "N/A";
		}
		
		if(isset($row['url'])){
			$url =  $row['url'];
		}
	        else{
			$url="N/A";
		}
	        
		if(isset($row['notes'])){
			$note = $row['notes'];
		}
	        else{
			$note = "N/A";
		}
	        
		if(isset($row['rem'])){
			$reminder =  $row['rem'];
		}
	        else{
			$reminder="N/A";
	    	}
	        
		if(isset($row['ad'])){
			if($row['ad'] == 'yes'){
				$allday = true;
			}
			else{
	    			$allday =  false;
			}
	    	}
	    	else{
			$allday = "N/A";
		}

	        $arr[] = array('id' => $event_id, 'cid' => $cal_id, 
                    'start' => $start, 
                    'end' => $end,'title' => $title,'loc' => $location, 
                    'url' => $url,'rem' => $reminder,'ad' => $allday,
                    'notes' => $note);

       		//array_push($bigArr,$arr);
	}

	return $arr;

}
