<!DOCTYPE html>
<?php 
ini_set("session.cookie_httponly", 1);
session_start(); 
?>
<html>
<head>
    <title>Calendar</title>
    <!-- Sets up all Ext and Extensible includes: -->
    <script type="text/javascript" src="extensible-1.5.2/Extensible-config.js"></script>
    
    <!-- Page-specific includes -->
    <script type="text/javascript" src="basic.js"></script>
    <style>
        .sample-ct {
            height: 100px;
        }
    </style>
</head>

<body>

<?php

if(isset($_POST['token'])){
$_SESSION['checkToken'] = $_POST['token'];
}
if($_SESSION['token'] != $_SESSION['checkToken']){
	die("Request forgery detected");
}

if (isset($_POST['name']) && isset($_POST['pw'])) {
$_SESSION['user'] = $_POST['name'];
$user = $_SESSION['user'];
$_SESSION['pass'] = $_POST['pw'];
$pw = $_SESSION['pass'];
} else if(isset($_SESSION['user'])){
$user = $_SESSION['user'];
$pw = $_SESSION['pass'];
}
else{
session_destroy();
header("Location: login.php?success=false");
exit;
}
$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'mod6');

if($mysqli->connect_errno) {
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
}
if(mysqli_num_rows(mysqli_query($mysqli, "SELECT name FROM registeredusers WHERE name = '$user'" )) >0 ){

	$result = mysqli_query($mysqli, "SELECT password FROM registeredusers WHERE name  = '$user'" );
$hashedPass = $result->fetch_row();

if(crypt($pw,$hashedPass[0]) != $hashedPass[0]){
		session_destroy();
		header("Location: login.php?success=false");

	exit;
	}

}else{
session_destroy();
header("Location: login.php?success=false");
exit;
}
$_SESSION['token2'] = substr(md5(rand()), 0, 10);
echo "Logged in as: ".$user;
?> 

<form name = "logout" action="logout.php" method ="POST">
<input type = "submit" value= "Log out"></form>

 <div id="simple" class="sample-ct"></div>

</body>

</html>
