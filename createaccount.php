<!DOCTYPE html>
<html>
<head><meta charset="UTF-8"><title>Create account</title></head>
<body>
<?php 
include 'database.php';
if(isset($_POST['user']) && isset($_POST['pw'])){
        if(!empty($_POST['user']) && !empty($_POST['pw'])){


                // Use a prepared statement
                $stmt = $mysqli->prepare("SELECT COUNT(*) FROM registeredusers WHERE name=?");


                $name=$_POST['user'];
                // Bind the parameter
                $stmt->bind_param('s', $name);
                //$user = $_POST['username'];
                $stmt->execute();

                // Bind the results
                $stmt->bind_result($cnt);
                $stmt->fetch();

                $pwd_set = $_POST['pw'];
                // Compare the submitted password to the actual password hash
                if( $cnt == 0 ){
                        // Login succeeded!

                        $pwd_hash = crypt($pwd_set);
                        register($name, $pwd_hash);
                        // Redirect to your target page
			header("Location: login.php?success=true");
			exit;
                }else{
                        // register failed;
//                      echo "register failed: username already existed.";
			header("Location: register.php?invalid=true");
			exit;
                }


        }
}
else{
        header("Location: register.php");
        exit;
}


 
function register($name, $pwd){
        require 'database.php';

        $stmt = $mysqli->prepare("insert into registeredusers (name, password) values (?, ?)");
        if(!$stmt){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
        }

        $stmt->bind_param('ss', $name, $pwd);

        $stmt->execute();

        $stmt->close();

//      echo "congratulation";
//      header('Location: index.php');
}


?>
</body>
</html>
